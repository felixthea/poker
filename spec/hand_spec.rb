# -*- coding: utf-8 -*-

require 'rspec'
require "hand"


describe Hand do

  subject(:hand) { Hand.new}
  let(:ace_spades) { double("ace_spades", :suit => :spades, :value => 14) }
  let(:two_clubs) { double("two_clubs", :suit => :clubs, :value => 2) }
  let(:two_diamonds) { double("two_diamonds", :suit => :diamonds, :value => 2) }
  let(:two_hearts) { double("two_hearts", :suit => :hearts, :value => 2) }
  let(:two_spades) { double("two_spades", :suit => :spades, :value => 2) }
  let(:three_spades) { double("three_spades", :suit => :spades, :value => 3) }
  let(:four_spades) { double("four_spades", :suit => :spades, :value => 4) }
  let(:five_spades) { double("five_spades", :suit => :spades, :value => 5) }
  let(:five_hearts) { double("five_hearts", :suit => :hearts, :value => 5) }

  context "when maintaining hand" do

    it "should start empty" do
      expect(hand.cards).to eq([])
    end

    it "should add cards to array" do
      hand.add_card(ace_spades)
      expect(hand.cards).to eq([ace_spades])
    end

    it "should be able to remove cards from hand" do
      hand.add_card(ace_spades)
      hand.add_card(two_hearts)
      
      hand.remove_card(ace_spades)
      expect(hand.cards).to eq([two_hearts])
    end
    
    it "should be able to remove multiple cards from hand" do
      hand.add_card(ace_spades)
      hand.add_card(two_hearts)
      
      hand.remove_card(ace_spades)
      hand.remove_card(two_hearts)
      
      expect(hand.cards).to eq([])
    end
    
    it "should know that it contains an ace" do
      hand.stub(:cards).and_return([ace_spades, two_clubs, three_spades, four_spades, five_spades])
      expect(hand.ace_exists?).to eq(true)
    end
    
  end

  context "when examining hands it should be able to" do

    it "identify a high card" do
      hand.stub(:cards).and_return([ace_spades, five_spades])
      expect(hand.compare).to eq([ace_spades])
    end
    
    it "identify a pair" do
      hand.stub(:cards).and_return([two_diamonds, three_spades, two_hearts])
      expect(hand.compare).to eq([two_diamonds, two_hearts])
    end
    
    it "identify two pair" do
      hand.stub(:cards).and_return([two_diamonds, three_spades, two_hearts, five_spades, five_hearts])
      expect(hand.compare).to eq([five_spades, five_hearts, two_diamonds, two_hearts])
    end
    
    it "identify three of a kind" do
      hand.stub(:cards).and_return([two_diamonds, three_spades, two_hearts, two_spades, five_hearts])
      expect(hand.compare).to eq([two_diamonds, two_hearts, two_spades])
    end
    
    it "identify a straight" do
      hand.stub(:cards).and_return([ace_spades, two_clubs, three_spades, four_spades, five_spades])
      expect(hand.compare).to eq([ace_spades, two_clubs, three_spades, four_spades, five_spades])
    end
    
    it "identify a flush" do
      hand.stub(:cards).and_return([ace_spades, two_spades, three_spades, four_spades, five_spades])
      expect(hand.compare).to eq([ace_spades, two_spades,three_spades, four_spades, five_spades])
    end
    
    it "identify a full house" do
      hand.stub(:cards).and_return([two_clubs, two_diamonds, two_hearts, five_spades, five_hearts])
      expect(hand.compare).to eq([two_clubs, two_diamonds, two_hearts, five_spades, five_hearts])
    end
    
    it "identify a four of a kind" do
      hand.stub(:cards).and_return([two_clubs, ace_spades, two_hearts, two_diamonds, two_spades])
      expect(hand.compare).to eq([two_clubs, two_hearts, two_diamonds, two_spades])
    end
    
    it "identify a straight flush" do
      hand.stub(:cards).and_return([ace_spades, two_spades, three_spades, four_spades, five_spades])
      expect(hand.compare).to eq([ace_spades, two_spades, three_spades, four_spades, five_spades])
    end

  end

  context "when picking the best hand" do
  
    it "should find its best 'hand'" do
      hand.stub(:cards).and_return([ace_spades, two_spades, three_spades, four_spades, five_spades])
      expect(hand.compare).to eq([ace_spades, two_spades, three_spades, four_spades, five_spades])
    end
    
    it "should pick a pair over a high card" do
      hand.stub(:cards).and_return([ace_spades, two_hearts, two_diamonds])
      expect(hand.best_hand).to eq([two_hearts, two_diamonds])
    end
    
    it "should pick a two pair over a pair" do
      hand.stub(:cards).and_return([ace_spades, two_clubs, two_hearts, five_spades, five_hearts])
      expect(hand.best_hand).to eq([five_spades, five_hearts, two_clubs, two_hearts])
    end
  
    it "should pick three of a kind over a pair" do
      hand.stub(:cards).and_return([two_hearts, two_clubs, two_diamonds])
      expect(hand.best_hand).to eq([two_hearts, two_clubs, two_diamonds])
    end
  
    it "should pick a full house over three of a kind" do
      hand.stub(:cards).and_return([two_hearts, two_clubs, two_diamonds, five_spades, five_hearts])
      expect(hand.best_hand).to eq([two_hearts, two_clubs, two_diamonds, five_spades, five_hearts])
    end
  
    it "should pick a four of a kind over three of a kind" do
      hand.stub(:cards).and_return([two_hearts, two_clubs, two_diamonds, two_spades, ace_spades])
      expect(hand.best_hand).to eq([two_hearts, two_clubs, two_diamonds, two_spades])
    end
  
    it "should pick straight flush over a flush" do
      hand.stub(:cards).and_return([ace_spades, two_spades, three_spades, four_spades, five_spades])
      expect(hand.compare).to eq([ace_spades, two_spades, three_spades, four_spades, five_spades])
    end
  
    it "should pick straight flush over a straight" do
      hand.stub(:cards).and_return([ace_spades, two_spades, three_spades, four_spades, five_spades])
      expect(hand.compare).to eq([ace_spades, two_spades, three_spades, four_spades, five_spades])
    end
  
  end
end
