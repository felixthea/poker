# -*- coding: utf-8 -*-

require 'rspec'
require 'card'

describe Card do
  subject(:card) { Card.new(:spades, :four) }

  it "should exist" do
    expect(card.class).to eq(Card)
  end

  it "should know its value" do
    expect(card.value).to eq(4)
  end

  it "should know its suit" do
    expect(card.suit).to eq('♠')
  end

end