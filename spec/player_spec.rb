require 'rspec'
require 'player'

describe Player do
  
  let(:game) { double("game", :pot => 1000, :highest_bet => 300) }
  
  subject(:player) { Player.new("Felix", 1500, game) }
  its(:name) { should eq("Felix")}
  its(:bankroll) { should eq(1500)} 
  
  describe "#convert_to_arr" do
    it "converts a user's input into an array of cards to discard" do
      expect(player.convert_to_arr("1,2,3")).to eq([1,2,3])
    end
  end
  
  describe "bet calculations" do
    
    it "should know current bet" do
      player.stub(:current_bet).and_return(500)
      expect(player.current_bet). to eq(500)
    end
    
    it "should calculate amount needed correctly" do
      player.stub(:game_highest_bet).and_return(200)
      player.stub(:player_bet).and_return(50)
      expect(player.amount_needed).to eq(150)
    end
    
    it "should update player_bet when seeing" do
      player.stub(:amount_needed).and_return(200)
      player.see_bet
      
      expect(player.player_bet).to eq(200)
    end
    
    it "should update player_bet when raising" do
      player.stub(:amount_needed).and_return(100) 
      player.stub(:get_raise_amt).and_return(200)
      
      player.raise_bet
      
      expect(player.player_bet).to eq(300)
    end
    
    it "should update player's bankroll when seeing" do
      player.stub(:amount_needed).and_return(100)
      
      player.see_bet
      
      expect(player.bankroll).to eq(1400)
    end
    
    it "should update player's bankroll when raising" do
      player.stub(:amount_needed).and_return(100)
      player.stub(:get_raise_amt).and_return(200)
      
      player.raise_bet
      
      expect(player.bankroll).to eq(1200)
    end
  end
end