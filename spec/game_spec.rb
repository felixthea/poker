require 'rspec'
require 'game'

describe Game do
  
  subject(:game) { Game.new }
  let(:player_a) { double("Player A", :name => "Felix", :bankroll => 1500, :game => game) }
  let(:player_b) { double("Player B", :name => "Granger", :bankroll => 2500, :game => game) }
  let(:player_c) { double("Player C", :name => "Sid", :bankroll => 3500, :game => game) }
  let(:real_player) { Player.new("Real Player", 1000, game)}
  let(:player_d) { double("Player D", :name => "Joe", :bankroll => 0, :game => game)}
  
  it "should have a deck" do
    expect(game.deck).to be_a(Deck)
  end
  
  it "should have players" do
    game.stub(:players).and_return([real_player])
    expect(game.players[0]).to be_a(Player)
  end 
  
  it "should end when there's only one player left" do
    game.stub(:players).and_return([player_c])
    
    expect(game.game_over?).to eq(true)
  end
  
  it "should remove a player if they are out of money" do
    game.stub(:players).and_return([player_c, player_d])
    
    game.update_players
    
    expect(game.players).to eq([player_c])
  end
  
  it "should know which player's turn for action"
  
  it "should know how much is in the pot"
  
  it "should know the current highest bet"
  
  it "should update the amount in the pot"
  
  it "should know who has the best hand"
  
end