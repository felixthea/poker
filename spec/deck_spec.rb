# -*- coding: utf-8 -*-

require 'rspec'
require 'deck'

describe Deck do

  subject(:deck) { Deck.new }
  let(:card) { Card.new(:clubs, :deuce) }
  let(:card2) { Card.new(:spades, :five) }

  it "should start with 52 cards" do
    expect(deck.cards.count).to eq(52)
  end

  it "all the cards should be unique" do
    expect(deck.cards.uniq.count).to eq(52)
  end

  it "should be able to deal from top" do
    expect(deck.deal.class).to eq(Card)
  end

  it "should be able to return a card to the bottom" do
    deck.return(card)
    expect(deck.cards.pop).to eq(card)
  end

  it "should be able to return multiple cards to the bottom" do
    deck.return(card)
    deck.return(card2)
    expect(deck.cards.pop(2)).to eq([card, card2])
  end

  it "should be able to shuffle" do
    deck.cards.unshift(card)
    deck.shuffle!
    expect(deck.cards.first).to_not eq(card)
  end

end