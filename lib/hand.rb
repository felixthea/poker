  # -*- coding: utf-8 -*-
#   
require './card'
class Hand

  attr_accessor :cards

  def initialize
    @cards = []
  end
  
  def add_card(card)
    cards << card
    
    nil
  end
  
  def remove_card(card)
    cards.delete_if { |my_card| my_card == card }
    
    nil
  end
  
  def best_hand
    compare
  end
  
  def render
    cards.each_with_index do |card, index|
      "#{index}) #{card.value} of #{card.suit}\n"
    end
  end

  def compare
    return straight_flush unless straight_flush.empty?
    return four_kind unless four_kind.empty?
    return full_house unless full_house.empty?
    return flush unless flush.empty?
    return straight unless straight.empty?
    return three_kind unless three_kind.empty?
    return two_pair unless two_pair.empty?
    return pair unless pair.empty?
    [high_card]
  end
  
  def dup
    dup_hand = Hand.new
    cards.each do |card|
      dup_hand.cards << card.dup
    end
    
    dup_hand
  end
  
  def straight_flush
    return cards if (!(self.straight.empty?) && !(self.flush.empty?))
    []
  end
  
  def four_kind
    four_kind_cards = cards.dup
    four_kind_cards.each do |card|
      counter = 0 
      four_arr = []
      four_kind_cards.each do |card2|
        if card.value == card2.value
          counter += 1
          four_arr << card2
          return four_arr if counter == 4
        end
      end
    end
    
    []
  end
  
  def full_house
    fh_cards = cards.dup
    
    three_kind_cards = three_kind(fh_cards)

    possible_pair = fh_cards - three_kind_cards

    pair_cards = pair(possible_pair)
    
    if (three_kind_cards + pair_cards).count == 5
      cards
    else
      []
    end
    
  end
  
  def flush
    flush_cards = cards.dup
    suit_count = Hash.new
    
    flush_cards.each do |card|
      if suit_count.has_key?(card.suit)
        suit_count[card.suit] += 1
      else
        suit_count[card.suit] = 1
      end
    end
    
    suit_count.each do |suit, count|
      return cards if count == 5
    end
    
    []    
  end

  def ace_exists?
    self.cards.any? { |card| card.value == 14 }
  end
      
  def straight
    straight_count = 1
    straight_cards = cards.dup
    straight_cards.sort! { |card1, card2| card2.value <=> card1.value } #sort from largest to smallest
    
    straight_cards.each_with_index do |card, index|
      break if index == straight_cards.count-1 #skips this calculation when it's the last card
      next_card = straight_cards[index+1]
      straight_count += 1 if card.value-next_card.value == 1
    end
    
    if straight_count == 5 # if a straight with ace as 14 is possible, return that
      return cards
    elsif ace_exists? # if straight with ace as 14 is not possible, but an ace exists, try it with ace as one
      values = []
      
      straight_cards.each do |card|
        values << card.value
      end
      
      (2..5).each do |num|
        return [] unless values.include?(num)
      end
      
      return cards      
    end
    
    []
  end

  def three_kind(three_kind_cards = cards)
    three_kind_cards.each do |card|
      counter = 0 
      three_arr = []
      three_kind_cards.each do |card2|
        if card.value == card2.value
          counter += 1
          three_arr << card2
          return three_arr if counter == 3
        end
      end
    end

    []
  end

  def two_pair
    two_pair_cards = cards.dup
    
    #get the first pair
    pair1 = pair(two_pair_cards)
    
    leftover = two_pair_cards - pair1

    pair2 = pair(leftover)
    
    two_pairs = pair1 + pair2
    
    if two_pairs.count == 4
      return two_pairs
    else
      []
    end
  end
  
  def pair(pair_cards = cards)
    pairs = []
    (0...pair_cards.length).each do |start_index|
      (start_index+1...pair_cards.length).each do |end_index|
        if pair_cards[start_index].value == pair_cards[end_index].value
          pairs = [pair_cards[start_index], pair_cards[end_index]]
        end
      end
    end

    pairs
  end

  def high_card
    high_cards = cards.dup
    highest_card = high_cards.first
    high_cards.each do |card|
      if card.value > highest_card.value
        highest_card = card
      end
    end

    highest_card
  end
end