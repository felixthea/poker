# -*- coding: utf-8 -*-

class Card
  SUIT_STRINGS = {
    :clubs    => "♣",
    :diamonds => "♦",
    :hearts   => "♥",
    :spades   => "♠"
  }

  VALUE_STRINGS = {
    :deuce => "2",
    :three => "3",
    :four  => "4",
    :five  => "5",
    :six   => "6",
    :seven => "7",
    :eight => "8",
    :nine  => "9",
    :ten   => "10",
    :jack  => "J",
    :queen => "Q",
    :king  => "K",
    :ace   => "A"
  }

  POKER_VALUE = {
    :deuce => 2,
    :three => 3,
    :four  => 4,
    :five  => 5,
    :six   => 6,
    :seven => 7,
    :eight => 8,
    :nine  => 9,
    :ten   => 10,
    :jack  => 11,
    :queen => 12,
    :king  => 13,
    :ace => 14 # ace can also be 1
  }

  attr_reader :suit, :value, :values, :suits

  def initialize(suit, value)
    @suit = suit
    @value = value
  end
  
  def dup
    Card.new(@suit, @value)
  end
  
  def value
    POKER_VALUE[@value]
  end

  def suit
    SUIT_STRINGS[@suit]
  end

  def self.values
    POKER_VALUE.keys
  end

  def self.suits
    SUIT_STRINGS.keys
  end
  
  def switch_ace
    if card.value == 1
      self.value = 14
    end
    
    if card.value == 14
      self.value = 1
    end
  end
  
  private
  
  def value=(val)
    @value = val
  end
  
end