require './hand'

class Player
  
  attr_reader :name
  attr_accessor :bankroll, :game, :hand, :player_bet
  
  def initialize(name, bankroll, game)
    @name = name
    @bankroll = bankroll
    @hand = Hand.new
    @game = game
    @player_bet = 0
  end
  
  def which_action
    # puts hand.render
    puts "Would you like to 1) fold 2) see or 3) see+raise? (ex: 1): "
    gets.chomp.to_i
  end
  
  def get_game_pot
    game.pot
  end
  
  def game_highest_bet
    game.highest_bet
  end
  
  def amount_needed
    game_highest_bet - player_bet
  end
  
  def fold_hand
    self.hand = Hand.new
  end
  
  def see_bet
    puts "There's #{get_game_pot} in the pot."
    self.player_bet = amount_needed
    puts "You bet #{player_bet} to match the highest bet."
    self.bankroll -= player_bet
    return player_bet
  end
  
  def get_raise_amt
    gets.chomp.to_i
  end
  
  def raise_bet
    puts "You have $#{bankroll}, you're seeing #{amount_needed}, how much more do you want to raise? (ex: 100): "
    raise_amt = get_raise_amt
    self.player_bet = player_bet + raise_amt + amount_needed
    self.bankroll -= player_bet
    raise_amt
  end
  
  def discard_which
    puts hand.render
    puts "Which cards would you like to discard? (ex: 1, 3, 5): "
    input = gets.chomp
    
    convert_to_arr(input)
  end
  
  def convert_to_arr(input)
    discard_cards = []
    
    input.split(",").each do |num_str|
      discard_cards << num_str.to_i
    end
    
    discard_cards
  end
  
  
end