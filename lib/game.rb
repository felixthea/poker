require './player'
require './deck'
require 'debugger'

class Game
  
  attr_accessor :deck, :players, :pot, :highest_bet
  
  def initialize
    @deck = Deck.new
    @players = []
    @pot = 0
    @highest_bet = 0
  end
  
  def add_player(name, bankroll)
    players << Player.new(name, bankroll, self)
  end

  def play
    
    until game_over?
      deal_cards
      
      current_players = players.dup
      
      # Round one
      until bets_match?(current_players, highest_bet)
        current_players = ask_each_player(current_players) #removes any players that folded
      end
      
      # Discard and deal out cards
      cards_to_return = ask_to_discard(current_players)
      return_cards(cards_to_return)
      
      current_players.each do |current_player|
        cards_needed = 5-current_player.hand.cards.count
        cards_needed.times { deal_card(current_player) }
      end
    end
  end
  
  def return_cards(cards_to_return)
    cards_to_return.each do |card|
      deck.return(card)
    end
  end
  
  def ask_to_discard(current_players)
    cards_to_return = []
    
    current_players.each do |current_player|
      cards_to_return += current_player.discard_which
    end
    
    cards_to_return
  end
    
  
  def bets_match?(current_players, highest_bet)
    return false if highest_bet == 0
    current_players.all? { |player| player.player_bet == highest_bet }
  end
  
  def ask_each_player(current_players)
    current_players.each do |current_player|
      puts "It's #{current_player.name}'s turn."
      choice = current_player.which_action
      case choice
      when 1 #fold
        current_player.hand.cards.each do |card|
          deck.return(card)
        end

        current_player.fold_hand
        current_players.delete(current_player)
      when 2 #see
        # debugger
        self.pot += current_player.see_bet
      when 3 #see and raise
        self.pot += highest_bet
        raise_amt = current_player.raise_bet
        self.pot += raise_amt
        self.highest_bet += raise_amt
      end
    end
    
    current_players
  end
  
  def deal_cards
    5.times do 
      players.each do |player|
        deal_card(player)
      end
    end
  end
  
  def deal_card(player)
    card = deck.cards.pop
    player.hand.add_card(card)
  end

  def update_players
    players.delete_if { |player| player.bankroll <= 0 }
  end
  
  def game_over?
    players.count == 1
  end

  def test
    self.add_player("Felix", 1500)
    self.add_player("Allison", 2000)
    self.add_player("Champ", 3000)
  end
end
