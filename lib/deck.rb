# -*- coding: utf-8 -*-
require './card'

class Deck

  def self.build_deck
    new_deck = []

    Card.suits.each do |suit|
      Card.values.each do |value|
        new_deck << Card.new(suit, value)
      end
    end

    new_deck
  end

  attr_accessor :cards

  def initialize(cards = Deck.build_deck)
    @cards = cards
  end

  def deal
    cards.shift
  end

  def return(card)
    cards << card
  end

  def shuffle!
    cards.shuffle!
  end

end